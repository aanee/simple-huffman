defmodule Huff do
  @doc"""
  Simple huffman encoder and decoder using Map and Enum.sort. Can use String or char list as input if input is run through Huff.data first. Otherwise it is strictly Char list.

  Char list:
  'voodoo' |> Huff.test
  
  String:
  "voodoo" |> Huff.data |> Huff.test
  
  Char lists with non-ascii symbols are shown as lists instead of strings when decoded.
  """
  def test(in_data) do
    freq_list = in_data |> freq
    huff_tree = freq_list |> tree
    enco_table = huff_tree |> enco_map
    enco_data = enco_table |> enco(in_data)
    deco_data = huff_tree |> deco(enco_data)
    {freq_list, huff_tree, enco_table, enco_data, deco_data}
  end

  def data(text) do
    case String.valid?(text) do
      true -> text |> String.codepoints
      _ -> text
    end
  end

  def freq(text), do: freq(%{}, text)
  def freq(freq_map, []), do: freq_map
  def freq(freq_map, [h|t]) do
    case freq_map[h] do
      nil -> freq_map |> Map.put(h,1)
      _ -> freq_map |> Map.update!(h, &(&1 + 1))
    end
    |> freq(t)
  end

  def tree_sort(list), do: list |> Enum.sort(fn {_,a}, {_,b} -> a <= b end) |> tree

  def tree([{c0, f0}, {c1, f1}]), do: {c0, c1}
  def tree([{c0, f0}, {c1, f1} | tail]), do: [{{c0, c1}, f0+f1} | tail] |> tree_sort
  def tree(freq_m), do: freq_m |> Map.to_list |> tree_sort

  def enco_map(huff), do: enco_map(huff, [])
  def enco_map({zero, one}, code) do
    Map.merge(enco_map(zero, code ++ [0]), enco_map(one, code ++ [1]))
  end
  def enco_map(char, code), do: %{char => code}
 
  def enco(_, []), do: []
  def enco(e_map, [h|t]), do: e_map[h] ++ enco(e_map, t)

  def deco_dig({zero, one}, [h|t]) do 
    case h do
      0 -> zero
      1 -> one
    end
    |> deco_dig(t)
  end 
  def deco_dig(char, rest), do: {rest, char}

  def deco(_, []), do: []
  def deco(huff_t, data) do 
    {rest, c} = deco_dig(huff_t, data)
    [c] ++ deco(huff_t, rest)
  end
end

